package ua.com.avolkov.testinternship.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import ua.com.avolkov.testinternship.model.Filter;
import ua.com.avolkov.testinternship.model.User;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by alex.vol on 28.06.2017.
 */
public class UserDaoImpl implements UserDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void addUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.persist(user);
    }

    public void updateUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.update(user);
    }

    public void removeUser(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.load(User.class, id);
        if (user != null) {
            session.delete(user);
        }
    }

    public User getUserById(int id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        return user;
    }

    public List<User> listUsers(Filter filter) {
        Session session = sessionFactory.getCurrentSession();

        CriteriaBuilder builder = session.getCriteriaBuilder();

        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> root = query.from(User.class);

        query.select(root);

        query.where(getExpression(filter, builder, root));

        List<User> userList = null;
        if (filter.getUsersPerPage().equals("all")) {
            userList = session.createQuery(query).getResultList();
        } else {
            int length = Integer.parseInt(filter.getUsersPerPage());
            int start = (filter.getPage() - 1) * length;
            userList = session.createQuery(query).setFirstResult(start).setMaxResults(length).getResultList();
        }
        return userList;
    }

    public long usersCount(Filter filter) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<User> root = query.from(User.class);
        query.select(builder.count(root)).where(getExpression(filter, builder, root));
        return session.createQuery(query).getSingleResult().longValue();
    }

    private Expression<Boolean> getExpression(Filter filter, CriteriaBuilder builder, Root<User> root) {
        Expression<Boolean> expression = builder.ge(root.<Integer>get("id"), 0);

        if (filter.getName() != null) {
            expression = builder.like(root.<String>get("name"), "%" + filter.getName() + "%");
        }

        if (filter.getUsersPerPage() == null)
            filter.setUsersPerPage("10");

        if (filter.getPage() < 1)
            filter.setPage(1);

        return expression;
    }

}