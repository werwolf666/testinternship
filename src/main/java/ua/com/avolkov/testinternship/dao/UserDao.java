package ua.com.avolkov.testinternship.dao;

import ua.com.avolkov.testinternship.model.Filter;
import ua.com.avolkov.testinternship.model.User;

import java.util.List;

/**
 * Created by alex.vol on 28.06.2017.
 */
public interface UserDao {
    void addUser(User user);

    void updateUser(User user);

    void removeUser(int id);

    User getUserById(int id);

    List<User> listUsers(Filter filter);

    long usersCount(Filter filter);

}
