package ua.com.avolkov.testinternship.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.com.avolkov.testinternship.dao.UserDao;
import ua.com.avolkov.testinternship.model.Filter;
import ua.com.avolkov.testinternship.model.User;

import java.util.List;

/**
 * Created by alex.vol on 28.06.2017.
 */
@Service
public class UserServiceImpl implements UserService {
    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Transactional
    public void addUser(User user) {
        userDao.addUser(user);
    }

    @Transactional
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Transactional
    public void removeUser(int id) {
        userDao.removeUser(id);
    }

    @Transactional
    public User getUserById(int id) {
        return userDao.getUserById(id);
    }

    @Transactional
    public List<User> listUsers(Filter filter) {
        return userDao.listUsers(filter);
    }

    @Transactional
    public long usersCount(Filter filter) {
        return userDao.usersCount(filter);
    }

}
