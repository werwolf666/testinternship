package ua.com.avolkov.testinternship.service;

import ua.com.avolkov.testinternship.model.Filter;
import ua.com.avolkov.testinternship.model.User;

import java.util.List;

/**
 * Created by alex.vol on 28.06.2017.
 */
public interface UserService {
    void addUser(User book);

    void updateUser(User book);

    void removeUser(int id);

    User getUserById(int id);

    List<User> listUsers(Filter filter);

    long usersCount(Filter filter);

}