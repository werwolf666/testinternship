package ua.com.avolkov.testinternship.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.com.avolkov.testinternship.model.Filter;
import ua.com.avolkov.testinternship.model.User;
import ua.com.avolkov.testinternship.service.UserService;

/**
 * Created by alex.vol on 28.06.2017.
 */
@SessionAttributes(value = "filter")
@Controller
public class UserController {
    private UserService userService;

    @Autowired(required = true)
    @Qualifier(value = "userService")
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("filter")
    public Filter getUsersFilter() {
        return new Filter();
    }

    @RequestMapping(value = "users")
    public String listUsers(Model model, @ModelAttribute(value = "filter") Filter filter) {
        return listUsers(model, filter, 1);
    }

    @RequestMapping(value = "users/{page}")
    public String listUsers(Model model, @ModelAttribute(value = "filter") Filter filter, @PathVariable("page") int page) {
        model.addAttribute("filter", filter);
        if (!model.containsAttribute("user")) {
            model.addAttribute("user", new User());
        }
        model.addAttribute("listUsers", this.userService.listUsers(filter));
        int pagesCount = 1;
        try {
            int usersPerPage = Integer.parseInt(filter.getUsersPerPage());
            pagesCount = (int) Math.ceil((double) userService.usersCount(filter) / usersPerPage);
        } catch (NumberFormatException e) {

        }
        model.addAttribute("pagesCount", pagesCount);
        return "users";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user) {
        if (user.getId() == 0) {
            this.userService.addUser(user);
        } else {
            this.userService.updateUser(user);
        }
        return "redirect:/users";
    }

    @RequestMapping(value = "/remove/{id}")
    public String removeUser(@PathVariable("id") int id) {
        this.userService.removeUser(id);
        return "redirect:/users";
    }

    @RequestMapping(value = "edit/{id}")
    public String editUser(Model model, @ModelAttribute(value = "filter") Filter filter, @PathVariable("id") int id) {
        model.addAttribute("user", this.userService.getUserById(id));
        return listUsers(model, filter);
    }


}