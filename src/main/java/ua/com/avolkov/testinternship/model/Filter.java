package ua.com.avolkov.testinternship.model;

/**
 * Created by wolf on 30.06.2017.
 */
public class Filter {
    private String name;
    private int page;
    private String usersPerPage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String  getUsersPerPage() {
        return usersPerPage;
    }

    public void setUsersPerPage(String usersPerPage) {
        this.usersPerPage = usersPerPage;
    }
}
