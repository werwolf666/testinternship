<%--
  Created by IntelliJ IDEA.
  User: alex.vol
  Date: 28.06.2017
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <title>Users</title>
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
</head>
<body>
<div class="container">
    <h3>Users</h3>
    <div class="add well">
    <form:form action="/add" commandName="user" cssClass="form-inline">
        <form:hidden path="id"/>
        <form:hidden path="createdDate"/>
        <div class="form-group">
            <form:label path="name">
                <spring:message text="Name"/>
            </form:label>
            <form:input path="name"
                        size="25"
                        cssClass="form-control"
                        required="true"/>
        </div>
        <div class="form-group">
            <form:label path="age">
                <spring:message text="Age"/>
            </form:label>
            <form:input path="age"
                        size="2"
                        pattern="\d+"
                        cssClass="form-control"
                        required="true"
                        title="positive number"/>
        </div>
        <div class="form-group">
            <form:label path="admin">
                <spring:message text="Admin"/>
            </form:label>
            <form:checkbox path="admin"/>
        </div>
        <div class="form-group">
            <c:if test="${!empty user.name}">
                <input type="submit" class="btn btn-primary"
                       value="<spring:message text="Edit User"/>"/>
            </c:if>
            <c:if test="${empty user.name}">
                <input type="submit" class="btn btn-primary"
                       value="<spring:message text="Add User"/>"/>
            </c:if>
        </div>
    </form:form>
    </div>
    <form:form action="${filterUsers}" commandName="filter">
        <form:label path="name">
            <spring:message text="Name filter"/>
        </form:label>
        <form:input path="name"
                    size="${stringSize}"/>
        <input type="submit" class="btn btn-primary"
               value="<spring:message text="Apply"/>"/>
        <jsp:include page="pager.jsp"/>

        <table class="table table-striped table-bordered">
            <tr>
                <th width="80">id</th>
                <th width="120">Name</th>
                <th width="120">Age</th>
                <th width="120">Is admin?</th>
                <th width="250">Create date</th>
                <th width="60">Edit</th>
                <th width="60">Delete</th>
            </tr>
            <c:forEach items="${listUsers}" var="user">
                <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.age}</td>
                    <td>
                        <c:if test="${user.admin == true}">Yes</c:if>
                        <c:if test="${user.admin == false}">No</c:if>
                    </td>
                    <td>${user.createdDate}</td>
                    <td><a href="<c:url value='/edit/${user.id}'/>">Edit</a></td>
                    <td><a href="<c:url value='/remove/${user.id}'/>">Delete</a></td>
                </tr>
            </c:forEach>
        </table>
    </form:form>
</div>
</body>
</html>
