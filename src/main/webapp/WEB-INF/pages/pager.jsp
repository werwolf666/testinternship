<%--
  Created by IntelliJ IDEA.
  User: wolf
  Date: 30.06.2017
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="users-pager">
    <br>
    <div> Users per page:
        <form:select path="usersPerPage"
                     size="1"
                     onchange="this.form.submit()">
            <option value="5" <c:if test="${filter.usersPerPage == '5'}">selected="selected"</c:if>>5</option>
            <option value="10" <c:if test="${filter.usersPerPage == '10'}">selected="selected"</c:if>>10</option>
            <option value="20" <c:if test="${filter.usersPerPage == '20'}">selected="selected"</c:if>>20</option>
            <option value="50" <c:if test="${filter.usersPerPage == '50'}">selected="selected"</c:if>>50</option>
            <option value="all" <c:if test="${filter.usersPerPage == 'all'}">selected</c:if>>All</option>
            <input type="hidden" name="page" value="${1}"/>
        </form:select>
    </div>
    <br>
    <c:if test="${pagesCount != 1}">
        <ul class="pagination">
            <li <c:if test="${filter.page < 2}">class="disabled"</c:if>>
                <a href="<c:url value='/users/1'/>"><<</a>
            </li>
            <li <c:if test="${filter.page < 2}">class="disabled"</c:if>>
                <a href="<c:url value='/users/${filter.page - 1}'/>"><</a>
            </li>

            <c:forEach begin="1"
                       end="${pagesCount}"
                       step="1"
                       varStatus="i">
                <li <c:if test="${i.index == filter.page}">class="active"</c:if>>
                        <a href="<c:url value='/users/${i.index}'/>">${i.index}</a>
                </li>
            </c:forEach>

            <li <c:if test="${filter.page >= pagesCount}">class="disabled"</c:if>>
                <a href="<c:url value='/users/${filter.page + 1}'/>">></a>
            </li>
            <li <c:if test="${filter.page >= pagesCount}">class="disabled"</c:if>>
                <a href="<c:url value='/users/${pagesCount}'/>">>></a>
            </li>
        </ul>
    </c:if>
</div>